
#pragma once

#include <memory>
#include <mongo/bson/bson.h>
#include <spdlog/spdlog.h>
#include <mongo/client/dbclient.h>

class MetaStorage
{
	mongo::DBClientConnection _mongo;
	std::shared_ptr<spdlog::logger> _log;
public:

	MetaStorage();
	~MetaStorage();

	void connect(std::string mongo_connection_string);

	mongo::BSONObj find(const mongo::BSONObj & query);
};

typedef std::shared_ptr<MetaStorage> MetaStoragePtr;
