
#pragma once

#include <vector>
#include <cstdint>
#include <handlers/hlsv3.h>

typedef std::vector<uint8_t> Buffer;

Buffer ts_multiplex(const std::vector<Hlsv3Handler::SegmentInfo> & es_list);
