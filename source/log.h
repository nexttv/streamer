
#pragma once

#include <unistd.h>
#include <spdlog/spdlog.h>
#include <spdlog/formatter.h>
#include <spdlog/details/os.h>

// pattern: spdlog::set_pattern("%Y.%m.%d %T%z %L [%15n] %v");
class LogFormatter : public spdlog::formatter
{

private:

	bool _print_colored_output = false;

	const char* _fixed_level_names(spdlog::level::level_enum l)
	{
		static const char* fixed_level_names[] = { "TRCE", "DEBG", "INFO", "NOTC", "WARN", "ERRR", "CRIT", "ALER", "EMER", "OFFF"};
		return fixed_level_names[l];
	}

	const char* _loglevel_color(spdlog::level::level_enum l)
	{
		static const char* fixed_level_names[] = { "2", "2", "1", "32", "33", "31", "31;1", "31", "31", "0"};
		return fixed_level_names[l];
	}

public:

	LogFormatter()
	{
		_print_colored_output = isatty(fileno(stdout));
	}

    void format(spdlog::details::log_msg& msg) override
    {
    	auto tm_time = spdlog::details::os::localtime(spdlog::log_clock::to_time_t(msg.time));
        auto duration = msg.time.time_since_epoch();
        auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count() % 1000;

        /* Slower version(while still very fast - about 3.2 million lines/sec under 10 threads),
        msg.formatted.write("[{:d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}.{:03d}] [{}] [{}] {} ",
        tm_time.tm_year + 1900,
        tm_time.tm_mon + 1,
        tm_time.tm_mday,
        tm_time.tm_hour,
        tm_time.tm_min,
        tm_time.tm_sec,
        static_cast<int>(millis),
        msg.logger_name,
        level::to_str(msg.level),
        msg.raw.str());*/

        // Faster (albeit uglier) way to format the line (5.6 million lines/sec under 10 threads)
        if(_print_colored_output)
        {
        	msg.formatted << "\033[" << _loglevel_color(msg.level) << 'm';
        }

        msg.formatted << static_cast<unsigned int>(tm_time.tm_year + 1900) << '.'
                      << fmt::pad(static_cast<unsigned int>(tm_time.tm_mon + 1), 2, '0') << '.'
                      << fmt::pad(static_cast<unsigned int>(tm_time.tm_mday), 2, '0') << ' '
                      << fmt::pad(static_cast<unsigned int>(tm_time.tm_hour), 2, '0') << ':'
                      << fmt::pad(static_cast<unsigned int>(tm_time.tm_min), 2, '0') << ':'
                      << fmt::pad(static_cast<unsigned int>(tm_time.tm_sec), 2, '0') << '.'
                      << fmt::pad(static_cast<unsigned int>(millis), 3, '0') << " ";

        msg.formatted << _fixed_level_names(msg.level) << " ";
        msg.formatted << '[' << fmt::pad(msg.logger_name.c_str(), 15, ' ') << "] ";
        msg.formatted << fmt::StringRef(msg.raw.data(), msg.raw.size());

        if(_print_colored_output)
        {
        	msg.formatted << "\033[0m";
        }

        msg.formatted << spdlog::details::os::eol();
    }

};
