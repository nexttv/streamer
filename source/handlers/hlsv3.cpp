
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <spdlog/spdlog.h>
#include <isom/isom.h>
#include <mpegts.h>
#include "hlsv3.h"

const boost::regex Hlsv3Handler::URL_REGEX = boost::regex("/v0/segs/ts/standard/none/([0-9a-zA-Z:,_]+)\\.ts");

class Hlsv3Handler::UrlInfo
{
public:
	std::vector<SegmentInfo> es_list;

	std::string str()
	{
		std::ostringstream os;
		for(size_t i =0; i<es_list.size(); ++i)
		{
			os << es_list[i].str();
			if(i+1 != es_list.size())
			{
				os << ",";
			}
		}
		return fmt::format("elementary segments list: [{}]", os.str());
	}
};

bool Hlsv3Handler::parse_request(std::string uri, UrlInfo & ui)
{
	boost::smatch m;
	auto r = boost::regex_match(uri, m, URL_REGEX);

	if(!r)
	{
		return false;
	}

	assert(m.size() == 2);

	std::vector<std::string> toks;

	auto m1 = m[1].str();
    boost::split(toks, m1, boost::is_any_of(","), boost::token_compress_on);
    for(auto & t: toks)
    {
		std::vector<std::string> etoks;
	    boost::split(etoks, t, boost::is_any_of(":"), boost::token_compress_on);

	    if(etoks.size() != 3)
	    {
	    	_log->warn("failed to parse uri as hlsv3: elementary segment description {} is not of form <track_id>:<sid_list>:<start_dts>, uri: {}", t, uri);
	    	return false;
	    }

	    try
	    {
   			auto si = SegmentInfo();
	   		si.track_id = boost::lexical_cast<uint32_t>(etoks[0]);
	   		si.start_dts = boost::lexical_cast<uint64_t>(etoks[2]);
	   		boost::split(si.sid_list, etoks[1], boost::is_any_of("_"), boost::token_compress_on);

	   		ui.es_list.push_back(si);
	   	}
	   	catch(std::exception & e)
	   	{
	    	_log->warn("failed to parse uri as hlsv3: elementary segment description {} is not of form <id>:<track_id>:<start_dts>: {}, uri: {}", t, e.what(), uri);
	    	return false;
	   	}
    }

	return true;
}

Hlsv3Handler::Hlsv3Handler(MetaStoragePtr meta_storage, BlobStoragePtr blob_storage)
{
	_meta_storage = meta_storage;
	_blob_storage = blob_storage;
	_log = spdlog::stdout_logger_mt("hlsv3");
}

bool Hlsv3Handler::read_es(SegmentInfo & s)
{
	for(auto & sid: s.sid_list)
	{
		Blob tmp_blob;
		auto r = _blob_storage->find(sid, tmp_blob);

		if(!r)
		{
			return false;
		}
		s.blob.insert(s.blob.end(), tmp_blob.begin(), tmp_blob.end());
	}

	return true;
}

int Hlsv3Handler::handle(struct mg_connection * conn, boost::smatch)
{
	auto ui = UrlInfo();
	auto r = parse_request(conn->uri, ui);

	if(!r)
	{
		mg_send_status(conn, 400);
		mg_printf_data(conn, "failed to parse hlsv3 uri");
		return MG_TRUE;
	}

	for(auto & es : ui.es_list)
	{
		r = read_es(es);

		if(!r)
		{
			_log->warn("failed to find segment blob for segment '{}' when meta found", es.str());
			mg_send_status(conn, 404);
			mg_printf_data(conn, "Not Found");
			return MG_TRUE;
		}
	}

	auto blob = multiplex(ui.es_list);

	mg_send_header(conn, "Content-Type", "video/MP2T");
	mg_send_data(conn, blob.data(), blob.size());

	return MG_TRUE;
}

Blob Hlsv3Handler::multiplex(const std::vector<SegmentInfo> & es_list)
{
	return ts_multiplex(es_list);
}
