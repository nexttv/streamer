
#include <except.h>
#include "BlobStorage.h"

BlobStorage::BlobStorage()
{
	_log = spdlog::stdout_logger_mt("blob-storage");

	cluster = nullptr;
	io = nullptr;
}

BlobStorage::~BlobStorage()
{
	if (io != nullptr)
	{
		rados_ioctx_destroy(io);
	}

	if (cluster != nullptr)
	{
		rados_shutdown(cluster);
	}
}

void BlobStorage::connect(std::string ceph_conf, std::string ceph_keyring)
{
	int ret = 0;
	uint64_t flags;

	ret = rados_create(&cluster, NULL);
	enforce<SystemExit>(ret == 0, fmt::format("couldn't initialize rados cluster handle: error {}", ret));
	_log->debug("created rados cluster handle");

	ret = rados_conf_read_file(cluster, ceph_conf.c_str());
	enforce<SystemExit>(ret == 0, fmt::format("couldn't read the Ceph configuration file: error {}", ret));
	_log->debug("read Ceph configuration file {}", ceph_conf);

	ret = rados_connect(cluster);
	enforce<SystemExit>(ret == 0, fmt::format("couldn't connect to ceph cluster: error {}", ret));
	_log->info("connected to ceph cluster");

	auto pool = "segments";

	ret = rados_pool_create(cluster, pool);
	enforce<SystemExit>(ret == 0, fmt::format("failed to create librados pool {}: error {}", pool, ret));

	ret = rados_ioctx_create(cluster, pool, &io);
	enforce<SystemExit>(ret == 0, fmt::format("failed to create librados ioctx for pool {}: error {}", pool, ret));
	_log->debug("created rados ioctx for pool {}", pool);
}

bool BlobStorage::find(const std::string & sid, std::vector<uint8_t> & outbuf)
{
	_log->debug("reading segment '{}' ...", sid);

	size_t off = 0;
	int ret = 0;
	int iters = 0;

	char buf[5*1024*1024];
	outbuf.clear();

	while ((ret = rados_read(io, sid.c_str(), buf, sizeof(buf), off)) > 0)
	{
		_log->trace("read chunk of size {}", ret);
		off += ret;
		++iters;
		outbuf.insert(outbuf.end(), buf, buf + ret);
	}

	_log->debug("read segment '{}' of size {} within {} iterations, last ret: {}", sid, outbuf.size(), iters, ret);

	return ret == 0 && outbuf.size() > 0;
}
