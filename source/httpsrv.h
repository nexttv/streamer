
#pragma once

#include <cstdint>
#include <boost/regex.hpp>
#include <functional>
#include "mg_utils.h"

class HttpServer
{
	typedef std::function<int (struct mg_connection *, const boost::smatch &)> HandlerFunc;

	struct HandlerEntry
	{
		boost::regex regex;
		HandlerFunc handler;

		HandlerEntry(boost::regex r, HandlerFunc h)
		{
			regex = r;
			handler = h;
		}
	};

	volatile bool _running_flag;
	std::vector<HandlerEntry> _handlers;
	std::shared_ptr<spdlog::logger> _log;

	int process_request(struct mg_connection *conn)
	{
		if(strcmp(conn->request_method, "GET") != 0)
		{
			mg_send_status(conn, 405);
			mg_printf_data(conn, "only GET method is supported\n");

			return MG_TRUE;
		}

		for(auto & h: _handlers)
		{
			boost::smatch m;
			auto r = boost::regex_match(std::string(conn->uri), m, h.regex);

			if(r)
			{
				return h.handler(conn, m);
			}
		}

		return MG_FALSE;
	}

	static int process_request_nothrow(struct mg_connection *conn)
	{
		auto self = (HttpServer*) conn->server_param;

		try
		{
			return self->process_request(conn);
		}
		catch(std::exception & e)
		{
			self->_log->error("exception caught in handler: {}", e.what());
			mg_send_status(conn, 500);
			mg_printf_data(conn, "ups... something went wrong\n%s", e.what());
			return MG_TRUE;
		}
	}

	static int event_handler(struct mg_connection *conn, enum mg_event ev)
	{
		switch(ev)
		{
			case MG_REQUEST: return process_request_nothrow(conn);
			default: return MG_FALSE;
		}
	}

public:

	HttpServer()
	{
		_log = spdlog::stdout_logger_mt("http");
	}

	void register_handler(std::string r, HandlerFunc h)
	{
		_handlers.emplace_back(boost::regex(r), h);
	}

	void run(int port)
	{
		_running_flag = true;

		char port_str[10];
		sprintf(port_str, "%d", port);

		struct mg_server *server = mg_create_server(this, &HttpServer::event_handler);
		mg_set_option(server, "listening_port", port_str);
		mg_set_option(server, "access_log_file", "/dev/stdout");

		while(_running_flag)
		{
			mg_poll_server(server, 1000);   // Infinite loop, Ctrl-C to stop
		}

		mg_destroy_server(&server);
	}

	void stop()
	{
		_running_flag = false;
	}
};
